const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');

const IS_PRODUCTION = process.env.NODE_ENV === 'production';
const IS_DEVELOPMENT = process.env.NODE_ENV === 'development';
const styleFileName = IS_PRODUCTION ? 'styles.[contenthash:10].css' : 'styles.css';
const bundleFileName = IS_PRODUCTION ? 'bundle.[hash:10].min.js' : 'bundle.js';

const entry = IS_PRODUCTION
  ? {app: ['./src/index.js']}
  : {app: [
    'react-hot-loader/patch',
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:8080',
    './src/index.js'
  ]};

const plugins = IS_PRODUCTION
  ? [
    new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false } }),
    new HTMLWebpackPlugin({
      template: 'index-template.html'
    })
  ] : [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ];

plugins.push(
  new webpack.DefinePlugin({
    IS_DEVELOPMENT: JSON.stringify(IS_DEVELOPMENT),
    IS_PRODUCTION: JSON.stringify(IS_PRODUCTION)
  }),
  new ExtractTextPlugin({
    filename: styleFileName,
    disable: IS_DEVELOPMENT
  })
);

const rules = [
  {
    test: /\.css$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: ['css-loader', 'postcss-loader']
    }),
    exclude: '/node_modules/'
  },
  {
    test: /\.scss$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: ['css-loader', 'sass-loader', 'postcss-loader']
    })
  },
  {
    test: /\.gif$/,
    use: ['url-loader?limit=10000&mimetype=image/gif']
  },
  {
    test: /\.jpg$/,
    use: ['url-loader?limit=10000&mimetype=image/jpg']
  },
  {
    test: /\.png$/,
    use: ['url-loader?limit=10000&mimetype=image/png']
  },
  {
    test: /\.svg(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    use: ['url-loader?limit=10000&mimetype=image/svg+xml']
  },
  {
    test: /\.(woff(2)?|ttf|eot)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    use: ['url-loader?limit=1']
  },
  {
    test: /\.js$/,
    use: [
      'babel-loader',
    ],
    include: path.join(__dirname, 'src')
  }
];

module.exports = {
  entry: entry,
  plugins: plugins,
  module: {
    rules: rules
  },
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: IS_PRODUCTION ? '/' : '/dist',
    filename: bundleFileName
  },
  devtool: IS_PRODUCTION ? false : 'eval-source-map'
};