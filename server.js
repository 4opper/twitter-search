var WebpackDevServer = require('webpack-dev-server');
var webpack = require('webpack');
var config = require('./webpack.config.js');
var path = require('path');

var compiler = webpack(config);
var port = 8080;

var server = new WebpackDevServer(compiler, {
  hot: true,
  filename: config.output.filename,
  publicPath: config.output.publicPath,
  stats: {
    colors: true
  },
  proxy: {
    '/api': {
      target: 'http://localhost:3000',
      secure: false
    }
  }
});

server.listen(port, 'localhost', function (error) {
  if (error) {
    console.error(error);
  } else {
    console.info(`==> WebpackDevServer is listening on port ${port}. Open up http://localhost:${port} in your browser.`);
  }
});