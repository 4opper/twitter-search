export const getTenTwits = (tag = 'javascript') => ({
  type: 'SEND_TWITS_REQUEST',
  payload: {
    url: `api/search/${tag}`
  }
});
