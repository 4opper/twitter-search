import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { withRouter } from 'react-router-dom';
import { getTenTwits } from '../../actions/search';
import Spinner from '../Spinner';

const mapStateToProps = (state) => {
  return {
    results: state.results
  };
};

const mapDispatchToProps = {
  getTenTwits
};

@connect(mapStateToProps, mapDispatchToProps)
@withRouter
export default class List extends Component {
  static propTypes = {
    results: PropTypes.object,
    isSearch: PropTypes.bool
  };

  componentWillMount() {
    if (!this.props.results.isInProgress) {
      // search reload case
      if (this.props.isSearch) {
        const query = queryString.parse(this.props.location.search);
        this.props.getTenTwits(query.q);
      } else {
        this.props.getTenTwits();
      }
    }
  }

  render() {
    const results = this.props.results;
    if (results.isInProgress || !results.results) {
      return <Spinner />;
    }

    return (<main className="container home">
      <div className="title">
        Here are 10 latests twits with <span className="tag">{results.results.search_metadata.query}</span> hashtag
      </div>

      {results.results.statuses.map((status, index) => {
        return <div key={index} className="twit">
          {/*{console.log(status)}*/}
          <img src={`${status.user.profile_image_url_https}`} className="avatar" alt="avatar"/>
          <div className="content">
            <div className="name">{status.user.name} <br/>
              <span className="nickName">{`@${status.user.screen_name}`}</span>
            </div>
            {status.text}
          </div>
        </div>
      })}
    </main>);
  }
}
