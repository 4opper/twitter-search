import { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { getTenTwits } from '../../actions/search';

const mapDispatchToProps = {
  getTenTwits
};

@connect(() => ({}), mapDispatchToProps)
@withRouter
export default class Header extends Component {
  static propTypes = {
    getTenTwits: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      tag: ''
    };
  }

  componentWillMount() {
    const query = queryString.parse(this.props.location.search);
    if (query.q) {
      this.setState({tag: query.q});
    }

    this.unlisten = this.props.history.listen(location => {
      const listendQuery = queryString.parse(location.search);
      this.setState({tag: listendQuery.q || ''});
      // can't use getTenTwits as callback in setState cuz to make default function parameter to work passed
      // parameter should be undefined, but if set this.state.tag's value to undefined there will be warnings in
      // console about uncontrolled inputs
      this.props.getTenTwits(listendQuery.q || undefined);
    });
  }

  componentWillUnmount() {
    this.unlisten();
    console.log('unmount');
  }

  changeHandler(e) {
    this.setState({tag: e.target.value});
  }

  buttonClickHandler() {
    // this.props.getTenTwits(this.state.tag);
    this.props.history.push({
      pathname: '/search',
      search: queryString.stringify({q: this.state.tag})
    });
  }

  homeClickHandler() {
    this.setState({tag: ''})
  }

  render() {
    return (<header className="header">
      <div className="container">
        <ul>
          <li onClick={::this.homeClickHandler}><Link to="/">Home</Link></li>
          <li><input type="text" value={this.state.tag} onChange={::this.changeHandler}/></li>
          <li><button className="btn-search" onClick={::this.buttonClickHandler}>Search</button></li>
        </ul>
      </div>
    </header>);
  }
}
