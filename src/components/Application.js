import Header from './Header';

const Application = ({children}) => {
  return (
    <div className="app">
      <Header />
      {children}
    </div>
  );
};

export default Application;
