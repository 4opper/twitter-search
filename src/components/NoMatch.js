const NoMatch = () => (
  <div>
    <h2>Nothing here</h2>
  </div>
);

export default NoMatch;
