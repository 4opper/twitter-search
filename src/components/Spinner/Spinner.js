import PropTypes from 'prop-types';
import ReactSvgSpinner from 'react-svg-spinner';

const Spinner = ({speed, thickness = 2, gap = 2, size = 40, color = '#708692'}) => (
  <div className="spinner">
    <ReactSvgSpinner speed={speed} thickness={thickness} gap={gap} size={String(size)} color={color}/>
  </div>
);

Spinner.propTypes = {
  speed: PropTypes.oneOf(['slow', 'fast']),
  color: PropTypes.string,
  gap: PropTypes.number,
  size: PropTypes.number,
  thickness: PropTypes.number
};

export default Spinner;
