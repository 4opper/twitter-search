import { Component } from 'react';
import { HashRouter } from 'react-router-dom';

import routes from '../routes';

export default class Root extends Component {
  render() {
    return (<HashRouter>{routes}</HashRouter>);
  }
}
