import { Component } from 'react';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import routes from '../routes';

export default class Root extends Component {
  render() {
    return (<Provider store={this.props.store}>
      <HashRouter>
        {routes}
      </HashRouter>
    </Provider>);
  }
};
