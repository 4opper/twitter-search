import { combineEpics } from 'redux-observable';
import { rxGetTwits } from './results';

const epics = [rxGetTwits];

const appEpic = combineEpics(...epics);

export default appEpic;
