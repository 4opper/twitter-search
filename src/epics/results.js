import {rxFetch} from '../utils/fetch';

export const rxGetTwits = rxFetch(
  'SEND_TWITS_REQUEST',
  'SEND_TWITS_SUCCESS',
  'SEND_TWITS_FAILED'
);
