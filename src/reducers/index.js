import { combineReducers } from 'redux';
import results from './results';

const appReducer = combineReducers({
  results
});

export default appReducer;
