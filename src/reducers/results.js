import {
  INITIAL_STATE,
  IN_PROGRESS_STATE,
  SUCCESS_STATE,
  FAILED_STATE
} from '../utils/redux';

const results = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SEND_TWITS_REQUEST':
      return {...state, ...IN_PROGRESS_STATE};
    case 'SEND_TWITS_SUCCESS':
      return {
        ...state, results: action.payload.data, ...SUCCESS_STATE
      };
    case 'SEND_TWITS_FAILED':
      return {...state, ...FAILED_STATE};
    default:
      return state;
  }
};

export default results;
