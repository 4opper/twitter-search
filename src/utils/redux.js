export const SEARCH_URL = '/api/search/';

/**
 * Redux
 */
export const INITIAL_STATE = {
  isSuccess: false,
  isInProgress: false,
  actionInProgress: false,

  error: null,
  actionError: null
};

export const IN_PROGRESS_STATE = {
  ...INITIAL_STATE,
  isInProgress: true
};

export const FAILED_STATE = {
  ...INITIAL_STATE,
  isFailed: true
};

export const SUCCESS_STATE = {
  ...INITIAL_STATE,
  isSuccess: true
};

// export const ACTION_IN_PROGRESS = {
//   ...INITIAL_STATE,
//   actionInProgress: true
// };
//
// export const ACTION_COMPLETED = {
//   ...INITIAL_STATE,
//   actionInProgress: false
// };
//
// export const error = (state, action) => ({
//   ...state,
//   ...FAILED_STATE,
//   error: action.payload
//   && (action.payload.error_description || action.payload.error || action.payload.message || 'Unknown')
// });
//
// export const actionError = (state, action) => ({
//   ...state,
//   ...ACTION_COMPLETED,
//   actionError: action.payload
//   && (action.payload.error_description || action.payload.error || action.payload.message || 'Unknown')
// });
