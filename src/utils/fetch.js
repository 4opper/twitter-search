import {of} from 'rxjs/observable/of';
import {ajax} from 'rxjs/observable/dom/ajax';

const defaultOptions = {
  timeout: 30000
};

function getHeaders() {
  return {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'dataType': 'json'
  };
}

export const rxFetch = (START, SUCCESS, FAILED, CANCEL = 'CANCEL_ALL', options) => {
  const {timeout = 30000} = {...defaultOptions, ...options};
  return (action$, store) =>
    action$.ofType(START)
      .switchMap(action => {
        const params = {
          url: action.payload.url,
          timeout: timeout,
          headers: {...getHeaders(), ...(action.payload.headers || {})},
          method: action.payload.method || 'GET'
        };
        if (action.payload.body) {
          if (params.headers['Content-type'] === 'application/json') { //TODO perform more reliable check
            params.body = JSON.stringify(action.payload.body);
          } else {
            params.body = action.payload.body;
          }
        }
        return ajax(params)
          .map(response => {
            const payload = (typeof response.response === 'object' && !Array.isArray(response.response))
              ? {...action.payload, ...response.response}
              : response.response;
            return {type: SUCCESS, payload};
          })
          .takeUntil(action$.ofType(CANCEL))
          .catch(error => of({
            status: error.status,
            type: FAILED,
            payload: {...action.payload, ...error.xhr.response},
            error: true
          }));
      });
};
