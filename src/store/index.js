import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { createEpicMiddleware } from 'redux-observable';
import appReducer from '../reducers';
import appEpic from '../epics';

const epicMiddleware = createEpicMiddleware(appEpic);

if (module.hot) {
  module.hot.accept('../epics', () => {
    epicMiddleware.replaceEpic(require('../epics').default);
  });
}

const enhancer = applyMiddleware(createLogger(), epicMiddleware);

export default function configureStore(initialState) {
  const store = createStore(appReducer, initialState, enhancer);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      store.replaceReducer(require('../reducers').default);
    });
  }
  return store;
};