import { Route, Redirect, Switch } from 'react-router-dom';

import Application from './components/Application';
import NoMatch from './components/NoMatch';
import List from './components/List';

export default <Route path="/">
  <Application>
    <Switch>
      <Route exact path="/" render={() => <Redirect to="/home"/>}/>
      <Route path="/home" component={List} />
      <Route path="/search" render={() => <List isSearch/>}/>
      <Route component={NoMatch}/>
    </Switch>
  </Application>
</Route>;
